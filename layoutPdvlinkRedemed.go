package layout_pdvlink_redemed

import (
	"bitbucket.org/infarma/layout-pdvlink-redemed/remessaDePedidos"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (remessaDePedidos.ArquivoDePedido	, error) {
	return remessaDePedidos.GetStruct(fileHandle)
}
