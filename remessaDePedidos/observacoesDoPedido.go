package remessaDePedidos

import (
	"errors"
	"strings"
)

type ObservacoesDoPedido struct {
	TipoDoRegistro            string `json:"TipoDoRegistro"`
	CnpjDoCliente             string `json:"CnpjDoCliente"`
	NumeroDoPedido            int64  `json:"NumeroDoPedido"`
	NumeroDaLinhaDeObservacao string `json:"NumeroDaLinhaDeObservacao"`
	MensagemDoPedido          string `json:"MensagemDoPedido"`
	ObservacaoDaNotaFiscal    string `json:"ObservacaoDaNotaFiscal"`
	Observacao                string `json:"Observacao"`
}

func (o *ObservacoesDoPedido) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 6 {
		o.TipoDoRegistro = items[0]
		o.CnpjDoCliente = items[1]
		o.NumeroDoPedido = retornaComoInt64(items[2])
		o.NumeroDaLinhaDeObservacao = items[3]
		o.MensagemDoPedido = items[4]
		o.ObservacaoDaNotaFiscal = items[5]
		o.Observacao = items[6]
	} else {
		return errors.New("Erro")
	}

	return nil
}
