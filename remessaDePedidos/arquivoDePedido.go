package remessaDePedidos

import (
	"bufio"
	"os"
	"strconv"
)

type ArquivoDePedido struct {
	HeaderDoArquivo     HeaderDoArquivo       `json:"HeaderDoArquivo"`
	CabecalhoDoPedido   CabecalhoDoPedido     `json:"CabecalhoDoPedido"`
	ItemDetalheDoPedido []ItemDetalheDoPedido `json:"ItemDetalheDoPedido"`
	ObservacoesDoPedido ObservacoesDoPedido   `json:"ObservacoesDoPedido"`
	RegistroDoCliente   RegistroDoCliente     `json:"RegistroDoCliente"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "00":
			err := arquivo.HeaderDoArquivo.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "01":
			err := arquivo.CabecalhoDoPedido.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "02":
			var registroTemp ItemDetalheDoPedido
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.ItemDetalheDoPedido = append(arquivo.ItemDetalheDoPedido, registroTemp)
			return arquivo, err
		case "08":
			err := arquivo.RegistroDoCliente.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "10":
			err := arquivo.ObservacoesDoPedido.ComposeStruct(fileScanner.Text())
			return arquivo, err
		}
	}
	return arquivo, err
}

func retornaComoInt64(valor string) int64 {
	vl, _ := strconv.ParseInt(valor, 10, 10)
	return vl
}

func retornaComoFloat64(value string, decimalPlaces int) float64 {
	some := value[:len(value)-decimalPlaces] + "." + value[len(value)-decimalPlaces:]
	result, _ := strconv.ParseFloat(some, 10)
	return result
}
