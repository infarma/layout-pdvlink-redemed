package remessaDePedidos

import (
	"errors"
	"strings"
)

type RegistroDoCliente struct {
	TipoDoRegistro                  int64  `json:"TipoDoRegistro"`
	CnpjDoCliente                   string `json:"CnpjDoCliente"`
	Nome                            string `json:"Nome"`
	NomeFantasia                    string `json:"NomeFantasia"`
	Endereco                        string `json:"Endereco"`
	Bairro                          string `json:"Bairro"`
	Cidade                          string `json:"Cidade"`
	Estado                          string `json:"Estado"`
	Cep                             string `json:"Cep"`
	Telefone                        string `json:"Telefone"`
	Contato                         string `json:"Contato"`
	CargoContato                    string `json:"CargoContato"`
	Email                           string `json:"Email"`
	DataDeCadastro                  string `json:"DataDeCadastro"`
	SugestaoDeLimiteDeCredito       string `json:"SugestaoDeLimiteDeCredito"`
	VencimentoDoLimiteDeCredito     string `json:"VencimentoDoLimiteDeCredito"`
	InscricaoEstadual               string `json:"InscricaoEstadual"`
	Suframa                         string `json:"Suframa"`
	ObservacaoDoCliente             string `json:"ObservacaoDoCliente"`
	ObservacaoDaNotaFiscalDoCliente string `json:"ObservacaoDaNotaFiscalDoCliente"`
}

func (r *RegistroDoCliente) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 6 {
		r.TipoDoRegistro = retornaComoInt64(items[0])
		r.CnpjDoCliente = items[1]
		r.Nome = items[2]
		r.NomeFantasia = items[3]
		r.Endereco = items[4]
		r.Bairro = items[5]
		r.Cidade = items[6]
		r.Estado = items[7]
		r.Cep = items[8]
		r.Telefone = items[9]
		r.Contato = items[10]
		r.CargoContato = items[11]
		r.Email = items[12]
		r.DataDeCadastro = items[13]
		r.SugestaoDeLimiteDeCredito = items[14]
		r.VencimentoDoLimiteDeCredito = items[15]
		r.InscricaoEstadual = items[16]
		r.Suframa = items[17]
		r.ObservacaoDoCliente = items[18]
		r.ObservacaoDaNotaFiscalDoCliente = items[19]
	} else {
		return errors.New("Erro")
	}

	return nil
}
