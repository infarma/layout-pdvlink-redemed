package retornoDePedidos

import (
	"errors"
	"strings"
)

type ItemDoPedido struct {
	TipoDoRegistro                 string `json:"TipoDoRegistro"`
	CnpjDoCliente                  string `json:"CnpjDoCliente"`
	NumeroDoPedido                 int64  `json:"NumeroDoPedido"`
	SequenciaDoItem                string `json:"SequenciaDoItem"`
	CodigoDoProduto                string `json:"CodigoDoProduto"`
	QuantidadeAtendida             int64  `json:"QuantidadeAtendida"`
	PosicaoDoItemDoPedido          string `json:"PosicaoDoItemDoPedido"`
	CodigoDoMotivoDeNaoAtendimento string `json:"CodigoDoMotivoDeNaoAtendimento"`
}

func (i *ItemDoPedido) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 8 {
		i.TipoDoRegistro = items[0]
		i.CnpjDoCliente = items[1]
		i.NumeroDoPedido = retornaComoInt64(items[2])
		i.SequenciaDoItem = items[3]
		i.CodigoDoProduto = items[4]
		i.QuantidadeAtendida = retornaComoInt64(items[5])
		i.PosicaoDoItemDoPedido = items[6]
		i.CodigoDoMotivoDeNaoAtendimento = items[7]
	} else {
		return errors.New("Erro")
	}

	return nil
}
