package retornoDePedidos

import (
	"bufio"
	"os"
	"strconv"
)

type RetornoDePedidos struct {
	HeaderDoArquivo   HeaderDoArquivo   `json:"HeaderDoArquivo"`
	ItemDoPedido      []ItemDoPedido    `json:"ItemDoPedido"`
	CabecalhoDoPedido CabecalhoDoPedido `json:"CabecalhoDoPedido"`
}

func GetStruct(fileHandle *os.File) (RetornoDePedidos, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := RetornoDePedidos{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "00":
			err := arquivo.HeaderDoArquivo.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "03":
			err := arquivo.CabecalhoDoPedido.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "04":
			var registroTemp ItemDoPedido
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.ItemDoPedido = append(arquivo.ItemDoPedido, registroTemp)
			return arquivo, err
		}
	}
	return arquivo, err
}

func retornaComoInt64(valor string) int64 {
	vl, _ := strconv.ParseInt(valor, 10, 10)
	return vl
}

func retornaComoFloat64(value string, decimalPlaces int) float64 {
	some := value[:len(value)-decimalPlaces] + "." + value[len(value)-decimalPlaces:]
	result, _ := strconv.ParseFloat(some, 10)
	return result
}
