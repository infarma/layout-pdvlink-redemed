package LayoutDoEstoqueDoDistribuidor

type DetalheEstoque struct {
	TipoDoRegistro          string	`json:"TipoDoRegistro"`
	CodigoDeBarrasDoProduto string	`json:"CodigoDeBarrasDoProduto"`
	QuantidadeDeEstoque     int64 	`json:"QuantidadeDeEstoque"`
}