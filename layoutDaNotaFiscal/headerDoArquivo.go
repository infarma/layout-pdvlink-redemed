package layoutDaNotaFiscal

import (
	"errors"
	"strings"
)

type HeaderDoArquivo struct {
	TIpoDoRegistro      string	`json:"TIpoDoRegistro"`
	CnpjDoDistribuidor  string	`json:"CnpjDoDistribuidor"`
	DataDoProcessamento string	`json:"DataDoProcessamento"`
	HoraDoProcessamento string	`json:"HoraDoProcessamento"`
}

func (h *HeaderDoArquivo) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 4 {
		h.TIpoDoRegistro = items[0]
		h.CnpjDoDistribuidor = items[1]
		h.DataDoProcessamento = items[2]
		h.HoraDoProcessamento = items[3]
	} else {
		return errors.New("Erro")
	}

	return nil
}
